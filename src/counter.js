import { useState } from "react"
import "./counter.css"

const Counter = () => {
    const [count, setCount] = useState(0)

    const decrease = () => {
        setCount(count - 1)
    }
    const increase = () => {
        setCount(count + 1)
    }
    return (
        <div className="counter">
            <button className="button" onClick={decrease}>-</button>
            {count}
            <button className="button" onClick={increase}>+</button>
        </div>
    )
}
export default Counter;