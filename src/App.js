import './App.css';
import MultipleCounter from './MultipleCounter';

function App() {
  return (
    <div className='container'>
      <MultipleCounter />
    </div>
  );
}

export default App;
