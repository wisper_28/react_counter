import { useState } from "react"
import CounterSizeGenerator from "./CounterSizeGenerator"
import CounterGroup from "./counterGroup"
const MultipleCounter = () => {
    const [num, setNum] = useState(0);
    function parentChange(data) {
        setNum(data)
    }
    return (
        <div>
            <CounterSizeGenerator parentChange = {parentChange} num = {num} />
            <CounterGroup  num={num} />
        </div>
    )
}
export default MultipleCounter;