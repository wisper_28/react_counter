import { useState } from "react"
const CounterSizeGenerator = ({parentChange, num}) => {
    const handleChange = (event) => {
        let inputVal = parseInt(event.target.value)
        console.log(inputVal);
        if(inputVal == NaN)
            inputVal = 0
        parentChange(inputVal)
    }
    return (
        <div>
        <span>size:</span>
        <input type="number" value={num} onChange={handleChange}></input>
        </div>
    )
}
export default CounterSizeGenerator;