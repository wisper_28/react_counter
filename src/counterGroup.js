import Counter from './counter';
import "./counterGroup.css"

const CounterGroup = ({num}) => {
    const countmap = new Array(num || 0).fill('');
    return (
        <div className='box'>
            {
            countmap.map(function(item, index) {
                return <Counter  key={index}/>
            })
        }
        </div>
    )
}
export default CounterGroup;